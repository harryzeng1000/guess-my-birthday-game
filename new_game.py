from random import randint
name = input("Hi! What is you name?")
# Guess #1
for guess_number in range(1,6):
    #it will run 5 times
    month_number = randint(1,12)
    year_number =randint(1970, 2004)

    print(f'Guess {guess_number} :{name} were you born in {month_number}/{year_number}?')
    #change back the Guess_1 to Guess
    # Add the guess number since I already create the variable for guess_number
    response = input("yes or no?").lower()
    if response == "yes":
        print("I knew it!")
        exit()
    elif guess_number == 5:
        #when the guess time is the last time
        print("I have other things to do. Good bye.")
    else:
        print("Drat! :emme try again!")
